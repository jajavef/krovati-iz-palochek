N0G00 G21 G17 G90
N10G00 G40 G49 G80
N20G71
N30T1M6
N40G43Z25.000H1M8
S18000M03
X0.000Y0.000
N70G00X844.000Y38.000Z25.000
N80G00Z18.000
N90G01Z4.000F1000.0
N100G00Z25.000
N110G00Y166.000
N120G00Z18.000
N130G01Z4.000F1000.0
N140G00Z25.000
N150G00X869.500Y37.884
N160G01Z16.000F1000.0
N170G01X869.467Y37.572Z15.703F3500.0
N180X869.361Y37.254Z15.387
N190X869.178Y36.956Z15.058
N200X868.928Y36.706Z14.725
N210X868.631Y36.524Z14.396
N220X868.313Y36.417Z14.080
N230X868.000Y36.384Z13.783
N240X867.687Y36.417Z13.487
N250X867.369Y36.524Z13.171
N260X867.072Y36.706Z12.842
N270X866.822Y36.956Z12.508
N280X866.639Y37.254Z12.179
N290X866.533Y37.572Z11.863
N300X866.500Y37.884Z11.566
N310X866.533Y38.197Z11.270
N320X866.639Y38.515Z10.954
N330X866.822Y38.813Z10.625
N340X867.072Y39.063Z10.291
N350X867.369Y39.245Z9.962
N360X867.687Y39.352Z9.646
N370X868.000Y39.384Z9.350
N380X868.313Y39.352Z9.053
N390X868.631Y39.245Z8.737
N400X868.928Y39.063Z8.408
N410X869.178Y38.813Z8.075
N420X869.361Y38.515Z8.000
N430G02X869.500Y37.884I-1.361J-0.631
N440G01X869.467Y37.572Z7.703
N450X869.361Y37.254Z7.387
N460X869.178Y36.956Z7.058
N470X868.928Y36.706Z6.725
N480X868.631Y36.524Z6.396
N490X868.313Y36.417Z6.080
N500X868.000Y36.384Z5.783
N510X867.687Y36.417Z5.487
N520X867.369Y36.524Z5.171
N530X867.072Y36.706Z4.842
N540X866.822Y36.956Z4.508
N550X866.639Y37.254Z4.179
N560X866.533Y37.572Z3.863
N570X866.500Y37.884Z3.566
N580X866.533Y38.197Z3.270
N590X866.639Y38.515Z2.954
N600X866.822Y38.813Z2.625
N610X867.072Y39.063Z2.291
N620X867.369Y39.245Z1.962
N630X867.687Y39.352Z1.646
N640X868.000Y39.384Z1.350
N650X868.313Y39.352Z1.053
N660X868.631Y39.245Z0.737
N670X868.928Y39.063Z0.408
N680X869.178Y38.813Z0.075
N690X869.361Y38.515Z0.000
N700G02X869.500Y37.884I-1.361J-0.631
N710G02X868.000Y36.384I-1.500J+0.000
N720G02X866.500Y37.884I-0.000J+1.500
N730G02X868.000Y39.384I+1.500J-0.000
N740G02X869.500Y37.884I+0.000J-1.500
N750G00Z25.000
N760G00Y198.000
N770G01Z16.000F1000.0
N780G01X869.463Y197.669Z15.686F3500.0
N790X869.351Y197.348Z15.366
N800X869.171Y197.063Z15.047
N810X868.937Y196.829Z14.736
N820X868.652Y196.649Z14.418
N830X868.331Y196.537Z14.097
N840X868.000Y196.500Z13.783
N850X867.687Y196.533Z13.487
N860X867.369Y196.639Z13.171
N870X867.072Y196.822Z12.842
N880X866.822Y197.072Z12.508
N890X866.639Y197.369Z12.179
N900X866.533Y197.687Z11.863
N910X866.500Y198.000Z11.566
N920X866.533Y198.313Z11.270
N930X866.639Y198.631Z10.954
N940X866.822Y198.928Z10.625
N950X867.072Y199.178Z10.291
N960X867.369Y199.361Z9.962
N970X867.687Y199.467Z9.646
N980X868.000Y199.500Z9.350
N990X868.313Y199.467Z9.053
N1000X868.631Y199.361Z8.737
N1010X868.928Y199.178Z8.408
N1020X869.178Y198.928Z8.075
N1030X869.361Y198.631Z8.000
N1040G02X869.500Y198.000I-1.361J-0.631
N1050G01X869.463Y197.669Z7.686
N1060X869.351Y197.348Z7.366
N1070X869.171Y197.063Z7.047
N1080X868.937Y196.829Z6.736
N1090X868.652Y196.649Z6.418
N1100X868.331Y196.537Z6.097
N1110X868.000Y196.500Z5.783
N1120X867.687Y196.533Z5.487
N1130X867.369Y196.639Z5.171
N1140X867.072Y196.822Z4.842
N1150X866.822Y197.072Z4.508
N1160X866.639Y197.369Z4.179
N1170X866.533Y197.687Z3.863
N1180X866.500Y198.000Z3.566
N1190X866.533Y198.313Z3.270
N1200X866.639Y198.631Z2.954
N1210X866.822Y198.928Z2.625
N1220X867.072Y199.178Z2.291
N1230X867.369Y199.361Z1.962
N1240X867.687Y199.467Z1.646
N1250X868.000Y199.500Z1.350
N1260X868.313Y199.467Z1.053
N1270X868.631Y199.361Z0.737
N1280X868.928Y199.178Z0.408
N1290X869.178Y198.928Z0.075
N1300X869.361Y198.631Z0.000
N1310G02X869.500Y198.000I-1.361J-0.631
N1320G02X868.000Y196.500I-1.500J-0.000
N1330G02X866.500Y198.000I+0.000J+1.500
N1340G02X868.000Y199.500I+1.500J-0.000
N1350G02X869.500Y198.000I+0.000J-1.500
N1360G00Z25.000
N1370G00Y326.000
N1380G01Z16.000F1000.0
N1390G01X869.467Y325.687Z15.703F3500.0
N1400X869.361Y325.369Z15.387
N1410X869.178Y325.072Z15.058
N1420X868.928Y324.822Z14.725
N1430X868.631Y324.639Z14.396
N1440X868.313Y324.533Z14.080
N1450X868.000Y324.500Z13.783
N1460X867.687Y324.533Z13.487
N1470X867.369Y324.639Z13.171
N1480X867.072Y324.822Z12.842
N1490X866.822Y325.072Z12.508
N1500X866.639Y325.369Z12.179
N1510X866.533Y325.687Z11.863
N1520X866.500Y326.000Z11.566
N1530X866.533Y326.313Z11.270
N1540X866.639Y326.631Z10.954
N1550X866.822Y326.928Z10.625
N1560X867.072Y327.178Z10.291
N1570X867.369Y327.361Z9.962
N1580X867.687Y327.467Z9.646
N1590X868.000Y327.500Z9.350
N1600X868.313Y327.467Z9.053
N1610X868.631Y327.361Z8.737
N1620X868.928Y327.178Z8.408
N1630X869.178Y326.928Z8.075
N1640X869.361Y326.631Z8.000
N1650G02X869.500Y326.000I-1.361J-0.631
N1660G01X869.467Y325.687Z7.703
N1670X869.361Y325.369Z7.387
N1680X869.178Y325.072Z7.058
N1690X868.928Y324.822Z6.725
N1700X868.631Y324.639Z6.396
N1710X868.313Y324.533Z6.080
N1720X868.000Y324.500Z5.783
N1730X867.687Y324.533Z5.487
N1740X867.369Y324.639Z5.171
N1750X867.072Y324.822Z4.842
N1760X866.822Y325.072Z4.508
N1770X866.639Y325.369Z4.179
N1780X866.533Y325.687Z3.863
N1790X866.500Y326.000Z3.566
N1800X866.533Y326.313Z3.270
N1810X866.639Y326.631Z2.954
N1820X866.822Y326.928Z2.625
N1830X867.072Y327.178Z2.291
N1840X867.369Y327.361Z1.962
N1850X867.687Y327.467Z1.646
N1860X868.000Y327.500Z1.350
N1870X868.313Y327.467Z1.053
N1880X868.631Y327.361Z0.737
N1890X868.928Y327.178Z0.408
N1900X869.178Y326.928Z0.075
N1910X869.361Y326.631Z0.000
N1920G02X869.500Y326.000I-1.361J-0.631
N1930G02X868.000Y324.500I-1.500J-0.000
N1940G02X866.500Y326.000I+0.000J+1.500
N1950G02X868.000Y327.500I+1.500J-0.000
N1960G02X869.500Y326.000I-0.000J-1.500
N1970G00Z25.000
N1980G00X1790.500Y326.000
N1990G01Z16.000F1000.0
N2000G01X1790.467Y325.687Z15.703F3500.0
N2010X1790.361Y325.369Z15.387
N2020X1790.178Y325.072Z15.058
N2030X1789.928Y324.822Z14.725
N2040X1789.631Y324.639Z14.396
N2050X1789.313Y324.533Z14.080
N2060X1789.000Y324.500Z13.783
N2070X1788.687Y324.533Z13.487
N2080X1788.369Y324.639Z13.171
N2090X1788.072Y324.822Z12.842
N2100X1787.822Y325.072Z12.508
N2110X1787.639Y325.369Z12.179
N2120X1787.533Y325.687Z11.863
N2130X1787.500Y326.000Z11.566
N2140X1787.533Y326.313Z11.270
N2150X1787.639Y326.631Z10.954
N2160X1787.822Y326.928Z10.625
N2170X1788.072Y327.178Z10.291
N2180X1788.369Y327.361Z9.962
N2190X1788.687Y327.467Z9.646
N2200X1789.000Y327.500Z9.350
N2210X1789.313Y327.467Z9.053
N2220X1789.631Y327.361Z8.737
N2230X1789.928Y327.178Z8.408
N2240X1790.178Y326.928Z8.075
N2250X1790.361Y326.631Z8.000
N2260G02X1790.500Y326.000I-1.361J-0.631
N2270G01X1790.467Y325.687Z7.703
N2280X1790.361Y325.369Z7.387
N2290X1790.178Y325.072Z7.058
N2300X1789.928Y324.822Z6.725
N2310X1789.631Y324.639Z6.396
N2320X1789.313Y324.533Z6.080
N2330X1789.000Y324.500Z5.783
N2340X1788.687Y324.533Z5.487
N2350X1788.369Y324.639Z5.171
N2360X1788.072Y324.822Z4.842
N2370X1787.822Y325.072Z4.508
N2380X1787.639Y325.369Z4.179
N2390X1787.533Y325.687Z3.863
N2400X1787.500Y326.000Z3.566
N2410X1787.533Y326.313Z3.270
N2420X1787.639Y326.631Z2.954
N2430X1787.822Y326.928Z2.625
N2440X1788.072Y327.178Z2.291
N2450X1788.369Y327.361Z1.962
N2460X1788.687Y327.467Z1.646
N2470X1789.000Y327.500Z1.350
N2480X1789.313Y327.467Z1.053
N2490X1789.631Y327.361Z0.737
N2500X1789.928Y327.178Z0.408
N2510X1790.178Y326.928Z0.075
N2520X1790.361Y326.631Z0.000
N2530G02X1790.500Y326.000I-1.361J-0.631
N2540G02X1789.000Y324.500I-1.500J+0.000
N2550G02X1787.500Y326.000I-0.000J+1.500
N2560G02X1789.000Y327.500I+1.500J-0.000
N2570G02X1790.500Y326.000I+0.000J-1.500
N2580G00Z25.000
N2590G00Y198.000
N2600G01Z16.000F1000.0
N2610G01X1790.463Y197.669Z15.686F3500.0
N2620X1790.351Y197.348Z15.366
N2630X1790.171Y197.063Z15.047
N2640X1789.937Y196.829Z14.736
N2650X1789.652Y196.649Z14.418
N2660X1789.331Y196.537Z14.097
N2670X1789.000Y196.500Z13.783
N2680X1788.687Y196.533Z13.487
N2690X1788.369Y196.639Z13.171
N2700X1788.072Y196.822Z12.842
N2710X1787.822Y197.072Z12.508
N2720X1787.639Y197.369Z12.179
N2730X1787.533Y197.687Z11.863
N2740X1787.500Y198.000Z11.566
N2750X1787.533Y198.313Z11.270
N2760X1787.639Y198.631Z10.954
N2770X1787.822Y198.928Z10.625
N2780X1788.072Y199.178Z10.291
N2790X1788.369Y199.361Z9.962
N2800X1788.687Y199.467Z9.646
N2810X1789.000Y199.500Z9.350
N2820X1789.313Y199.467Z9.053
N2830X1789.631Y199.361Z8.737
N2840X1789.928Y199.178Z8.408
N2850X1790.178Y198.928Z8.075
N2860X1790.361Y198.631Z8.000
N2870G02X1790.500Y198.000I-1.361J-0.631
N2880G01X1790.463Y197.669Z7.686
N2890X1790.351Y197.348Z7.366
N2900X1790.171Y197.063Z7.047
N2910X1789.937Y196.829Z6.736
N2920X1789.652Y196.649Z6.418
N2930X1789.331Y196.537Z6.097
N2940X1789.000Y196.500Z5.783
N2950X1788.687Y196.533Z5.487
N2960X1788.369Y196.639Z5.171
N2970X1788.072Y196.822Z4.842
N2980X1787.822Y197.072Z4.508
N2990X1787.639Y197.369Z4.179
N3000X1787.533Y197.687Z3.863
N3010X1787.500Y198.000Z3.566
N3020X1787.533Y198.313Z3.270
N3030X1787.639Y198.631Z2.954
N3040X1787.822Y198.928Z2.625
N3050X1788.072Y199.178Z2.291
N3060X1788.369Y199.361Z1.962
N3070X1788.687Y199.467Z1.646
N3080X1789.000Y199.500Z1.350
N3090X1789.313Y199.467Z1.053
N3100X1789.631Y199.361Z0.737
N3110X1789.928Y199.178Z0.408
N3120X1790.178Y198.928Z0.075
N3130X1790.361Y198.631Z0.000
N3140G02X1790.500Y198.000I-1.361J-0.631
N3150G02X1789.000Y196.500I-1.500J+0.000
N3160G02X1787.500Y198.000I-0.000J+1.500
N3170G02X1789.000Y199.500I+1.500J-0.000
N3180G02X1790.500Y198.000I-0.000J-1.500
N3190G00Z25.000
N3200G00X1790.500Y38.000
N3210G01Z16.000F1000.0
N3220G01X1790.467Y37.687Z15.703F3500.0
N3230X1790.361Y37.369Z15.387
N3240X1790.178Y37.072Z15.058
N3250X1789.928Y36.822Z14.725
N3260X1789.631Y36.639Z14.396
N3270X1789.313Y36.533Z14.080
N3280X1789.000Y36.500Z13.783
N3290X1788.687Y36.533Z13.487
N3300X1788.369Y36.639Z13.171
N3310X1788.072Y36.822Z12.842
N3320X1787.822Y37.072Z12.508
N3330X1787.639Y37.369Z12.179
N3340X1787.533Y37.687Z11.863
N3350X1787.500Y38.000Z11.566
N3360X1787.533Y38.313Z11.270
N3370X1787.639Y38.631Z10.954
N3380X1787.822Y38.928Z10.625
N3390X1788.072Y39.178Z10.291
N3400X1788.369Y39.361Z9.962
N3410X1788.687Y39.467Z9.646
N3420X1789.000Y39.500Z9.350
N3430X1789.313Y39.467Z9.053
N3440X1789.631Y39.361Z8.737
N3450X1789.928Y39.178Z8.408
N3460X1790.178Y38.928Z8.075
N3470X1790.361Y38.631Z8.000
N3480G02X1790.500Y38.000I-1.361J-0.631
N3490G01X1790.467Y37.687Z7.703
N3500X1790.361Y37.369Z7.387
N3510X1790.178Y37.072Z7.058
N3520X1789.928Y36.822Z6.725
N3530X1789.631Y36.639Z6.396
N3540X1789.313Y36.533Z6.080
N3550X1789.000Y36.500Z5.783
N3560X1788.687Y36.533Z5.487
N3570X1788.369Y36.639Z5.171
N3580X1788.072Y36.822Z4.842
N3590X1787.822Y37.072Z4.508
N3600X1787.639Y37.369Z4.179
N3610X1787.533Y37.687Z3.863
N3620X1787.500Y38.000Z3.566
N3630X1787.533Y38.313Z3.270
N3640X1787.639Y38.631Z2.954
N3650X1787.822Y38.928Z2.625
N3660X1788.072Y39.178Z2.291
N3670X1788.369Y39.361Z1.962
N3680X1788.687Y39.467Z1.646
N3690X1789.000Y39.500Z1.350
N3700X1789.313Y39.467Z1.053
N3710X1789.631Y39.361Z0.737
N3720X1789.928Y39.178Z0.408
N3730X1790.178Y38.928Z0.075
N3740X1790.361Y38.631Z0.000
N3750G02X1790.500Y38.000I-1.361J-0.631
N3760G02X1789.000Y36.500I-1.500J+0.000
N3770G02X1787.500Y38.000I-0.000J+1.500
N3780G02X1789.000Y39.500I+1.500J-0.000
N3790G02X1790.500Y38.000I+0.000J-1.500
N3800G00Z25.000
N3810G00X1790.500Y560.000
N3820G01Z16.000F1000.0
N3830G01X1790.467Y559.687Z15.703F3500.0
N3840X1790.361Y559.369Z15.387
N3850X1790.178Y559.072Z15.058
N3860X1789.928Y558.822Z14.725
N3870X1789.631Y558.639Z14.396
N3880X1789.313Y558.533Z14.080
N3890X1789.000Y558.500Z13.783
N3900X1788.687Y558.533Z13.487
N3910X1788.369Y558.639Z13.171
N3920X1788.072Y558.822Z12.842
N3930X1787.822Y559.072Z12.508
N3940X1787.639Y559.369Z12.179
N3950X1787.533Y559.687Z11.863
N3960X1787.500Y560.000Z11.566
N3970X1787.533Y560.313Z11.270
N3980X1787.639Y560.631Z10.954
N3990X1787.822Y560.928Z10.625
N4000X1788.072Y561.178Z10.291
N4010X1788.369Y561.361Z9.962
N4020X1788.687Y561.467Z9.646
N4030X1789.000Y561.500Z9.350
N4040X1789.313Y561.467Z9.053
N4050X1789.631Y561.361Z8.737
N4060X1789.928Y561.178Z8.408
N4070X1790.178Y560.928Z8.075
N4080X1790.361Y560.631Z8.000
N4090G02X1790.500Y560.000I-1.361J-0.631
N4100G01X1790.467Y559.687Z7.703
N4110X1790.361Y559.369Z7.387
N4120X1790.178Y559.072Z7.058
N4130X1789.928Y558.822Z6.725
N4140X1789.631Y558.639Z6.396
N4150X1789.313Y558.533Z6.080
N4160X1789.000Y558.500Z5.783
N4170X1788.687Y558.533Z5.487
N4180X1788.369Y558.639Z5.171
N4190X1788.072Y558.822Z4.842
N4200X1787.822Y559.072Z4.508
N4210X1787.639Y559.369Z4.179
N4220X1787.533Y559.687Z3.863
N4230X1787.500Y560.000Z3.566
N4240X1787.533Y560.313Z3.270
N4250X1787.639Y560.631Z2.954
N4260X1787.822Y560.928Z2.625
N4270X1788.072Y561.178Z2.291
N4280X1788.369Y561.361Z1.962
N4290X1788.687Y561.467Z1.646
N4300X1789.000Y561.500Z1.350
N4310X1789.313Y561.467Z1.053
N4320X1789.631Y561.361Z0.737
N4330X1789.928Y561.178Z0.408
N4340X1790.178Y560.928Z0.075
N4350X1790.361Y560.631Z0.000
N4360G02X1790.500Y560.000I-1.361J-0.631
N4370G02X1789.000Y558.500I-1.500J+0.000
N4380G02X1787.500Y560.000I+0.000J+1.500
N4390G02X1789.000Y561.500I+1.500J-0.000
N4400G02X1790.500Y560.000I-0.000J-1.500
N4410G00Z25.000
N4420G00X32.333Y36.137
N4430G01Z3.300F2000.0
N4440G02X31.500Y38.000I+1.667J+1.863F3500.0
N4450G02X34.000Y40.500I+2.500J-0.000
N4460G02X36.500Y38.000I-0.000J-2.500
N4470G02X34.000Y35.500I-2.500J+0.000
N4480G02X32.333Y36.137I+0.000J+2.500
N4490G01X30.666Y34.274
N4500G02X29.000Y38.000I+3.334J+3.726
N4510G02X34.000Y43.000I+5.000J+0.000
N4520G02X39.000Y38.000I+0.000J-5.000
N4530G02X34.000Y33.000I-5.000J+0.000
N4540G02X30.666Y34.274I-0.000J+5.000
N4550G00Z25.000
N4560G00X33.577Y195.536
N4570G01Z3.300F2000.0
N4580G02X31.500Y198.000I+0.423J+2.464F3500.0
N4590G02X34.000Y200.500I+2.500J+0.000
N4600G02X36.500Y198.000I+0.000J-2.500
N4610G02X34.000Y195.500I-2.500J+0.000
N4620G01X33.577Y195.536
N4630X33.154Y193.072
N4640G02X29.000Y198.000I+0.846J+4.928
N4650G02X34.000Y203.000I+5.000J+0.000
N4660G02X39.000Y198.000I+0.000J-5.000
N4670G02X34.000Y193.000I-5.000J+0.000
N4680G01X33.154Y193.072
N4690G00Z25.000
N4700G00X33.741Y323.513
N4710G01Z3.300F2000.0
N4720G02X31.500Y326.000I+0.259J+2.487F3500.0
N4730G02X34.000Y328.500I+2.500J-0.000
N4740G02X36.500Y326.000I+0.000J-2.500
N4750G02X34.000Y323.500I-2.500J+0.000
N4760G01X33.741Y323.513
N4770X33.481Y321.027
N4780G02X29.000Y326.000I+0.519J+4.973
N4790G02X34.000Y331.000I+5.000J-0.000
N4800G02X39.000Y326.000I-0.000J-5.000
N4810G02X34.000Y321.000I-5.000J-0.000
N4820G01X33.481Y321.027
N4830G00Z25.000
N4840G00X33.848Y557.505
N4850G01Z3.300F2000.0
N4860G02X31.500Y560.000I+0.152J+2.495F3500.0
N4870G02X34.000Y562.500I+2.500J+0.000
N4880G02X36.500Y560.000I-0.000J-2.500
N4890G02X34.000Y557.500I-2.500J-0.000
N4900G01X33.848Y557.505
N4910X33.697Y555.009
N4920G02X29.000Y560.000I+0.303J+4.991
N4930G02X34.000Y565.000I+5.000J+0.000
N4940G02X39.000Y560.000I-0.000J-5.000
N4950G02X34.000Y555.000I-5.000J-0.000
N4960G01X33.697Y555.009
N4970G00Z25.000
N4980G00X0.000Y0.000
N4990G00Z25.000
N5000M09
N5010G00X0.000Y0.000
N5020M30
